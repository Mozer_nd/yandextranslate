package ru.testing;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import ru.testing.entities.Translate;
import ru.testing.gateway.YandexTranslateGateway;
import java.util.ArrayList;
import java.util.Arrays;

class YandexTranslateTest {
    private String TRANSLATIONS_1 = "Привет";
    private String TRANSLATIONS_2 = "Мир!";

    @Test
    @DisplayName("Перевод \"Hello world\"")
    public void getTranslateHelloWorld(){
        YandexTranslateGateway yandexTranslateGateway = new YandexTranslateGateway();

        String s = "Hello World!";
        ArrayList<String> text = new ArrayList<String>(Arrays.asList(s.split(" ")));

        Translate translate = yandexTranslateGateway.getTranslate(text);

        Assertions.assertEquals(translate.getTranslations().get(0).getText(), TRANSLATIONS_1);
        Assertions.assertEquals(translate.getTranslations().get(1).getText(), TRANSLATIONS_2);
    }

}