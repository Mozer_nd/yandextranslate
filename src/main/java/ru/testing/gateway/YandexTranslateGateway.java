package ru.testing.gateway;

import com.google.gson.Gson;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.Unirest;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.json.JSONObject;
import ru.testing.entities.Translate;
import java.io.File;
import java.util.ArrayList;

@Slf4j
public class YandexTranslateGateway {
    private static final String URL = "https://translate.api.cloud.yandex.net/translate/v2/translate";
    private static final String IAM_TOKEN = "t1.9euelZqanIvKjZaMnJuWkcqJi8-Yxu3rnpWaiZ6bmcmVzsqAQ..."; // set your IAM-token

    @SneakyThrows
    public Translate getTranslate(ArrayList<String> text) {
        Gson gson = new Gson();

        JSONObject json = new JSONObject();
        json.put("folderId", "b1g2hpn4ss9b9qu15rl0");
        json.put("texts", text);
        json.put("targetLanguageCode", "ru");

        HttpResponse<String> response = Unirest
                .post(URL)
                .header("Content-Type:", "application/json")
                .header("Authorization: ", "Bearer " + IAM_TOKEN)
                .body(json)
                .asString();

        String strResponse = response.getBody();
        log.info("\n\nresponse: \n" + strResponse);

        return gson.fromJson(strResponse, Translate.class);
    }
}
