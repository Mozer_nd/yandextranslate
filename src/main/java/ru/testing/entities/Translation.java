package ru.testing.entities;

import com.google.gson.annotations.SerializedName;
import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class Translation {
    @SerializedName("text")
    private String text;
    @SerializedName("detectedLanguageCode")
    private String detectedLanguageCode;
}
