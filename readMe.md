## Test Framework for your API

### Presets
* install jdk8
* install lombok plugin
* customize lombok plugin (Build, Execution, Deployment --> Compiler --> Annotation Processors, Enable annotation processing)
* enjoy...

>if you want to run a YandexTranslateTest, generate and insert the token into YandexTranslateGateway https://cloud.yandex.ru/docs/translate/quickstart
